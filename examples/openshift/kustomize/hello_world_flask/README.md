# Example: Hello_world_flask in dev and prod env



1. Change the directory
```bash
cd ~/workshop/examples/openshift/kustomize/hello_world_flask/
```

![dir-structure](images/kustomize_hello_world_flask_white.png)


2. Create new namespace test-kustomize
```bash
oc new-projecte test-kustomize
```

3. Create an application with production settings
```bash
oc apply -k overlays/production
```

4. Check pods
```bash
oc get all
```

5. Check the application by sending a request in loop
```bash
oc expose svc prod-myapp
```
```bash
while true; do curl http://$(oc get route/prod-myapp -o jsonpath='{.status.ingress[0].host}')/ ; sleep .3; done
```

6. In the second terminal show logs
```bash
stern prod-myapp
```

7. Start new application in dev env
```bash
kubectl apply -k overlays/development
```

8. Check pods
```bash 
kubectl get all
```

9. Check the application by sending a request in loop
```bash
oc expose svc dev-myapp
```

```bash
while true; do curl http://$(oc get route/dev-myapp -o jsonpath='{.status.ingress[0].host}')/ ; sleep .3; done
```

10. In the second terminal show logs
```bash
stern dev-myapp
```

11. Clean
```bash
oc delete project test-kustomize
```

[go to home](../../../../README.md)

