# Images layer structure


## Create directory for data:
```bash
mkdir ~/oci && cd ~/oci
```

## Pull images
```bash 
skopeo copy docker://saschagrunert/mysterious-image oci:mysterious-image

```
or

## Check image directory
```bash
tree mysterious-image
```
output:
```bash
mysterious-image
├── blobs
│   └── sha256
│       ├── 0503825856099e6adb39c8297af09547f69684b7016b7f3680ed801aa310baaa
│       ├── 6d8c9f2df98ba6c290b652ac57151eab8bcd6fb7574902fbd16ad9e2912a6753
│       ├── 703c711516a88a4ec99d742ce59e1a2f0802a51dac916975ab6f2b60212cd713
│       └── 8b2633baa7e149fe92e6d74b95e33d61b53ba129021ba251ac0c7ab7eafea825
├── index.json
└── oci-layout
```

## Check index file
```bash
jq . mysterious-image/index.json
```

## Check manifest file wrote in index
```bash
jq . mysterious-image/blobs/sha256/703c711516a88a4ec99d742ce59e1a2f0802a51dac916975ab6f2b60212cd713
```

The image manifest provides the location to the configuration and set of layers for a single container image for a specific architecture and operating system. The size field, as the name stated, indicates the overall size of the object.

## Go deeper and check layer:
```bash
jq . mysterious-image/blobs/sha256/8b2633baa7e149fe92e6d74b95e33d61b53ba129021ba251ac0c7ab7eafea825
```

On the top of the JSON we find some metadata, like the created date, the architecture and the os of the image. The environment (Env) as well as the command (Cmd) to be executed can be found in the config section of the configuration. This section can contain even more parameters, like the working directory (WorkingDir), the User which should run the container process or the Entrypoint. This means, if you create an container image via a Dockerfile, then all these parameters will be converted into the JSON based configuration.

## Filesystem
Now, let’s have a look at the first layer, where we would expect the root filesystem (rootfs):
```bash
mkdir rootfs
tar -C rootfs -xf mysterious-image/blobs/sha256/0503825856099e6adb39c8297af09547f69684b7016b7f3680ed801aa310baaa
tree -L 1 rootfs/
```

output
```bash
rootfs/
├── bin
├── dev
├── etc
├── home
├── lib
├── media
├── mnt
├── opt
├── proc
├── root
├── run
├── sbin
├── srv
├── sys
├── tmp
├── usr
└── var
```

Let’s see what the other layer contains:
```bash
mkdir layer
tar -C layer -xf mysterious-image/blobs/sha256/6d8c9f2df98ba6c290b652ac57151eab8bcd6fb7574902fbd16ad9e2912a6753
tree layer
```

output:
```bash
layer
└── my-file
```

## How it will look when we use docker

### Pull and save image as archive
```bash
docker pull saschagrunert/mysterious-image
docker save saschagrunert/mysterious-image -o mysterious-image.tar
```

### Extract archive:

```bash
mkdir mysterious-image
tar -C mysterious-image -xf mysterious-image.tar
```

### check manifest file:
```bash
jq . mysterious-image/8b2633baa7e149fe92e6d74b95e33d61b53ba129021ba251ac0c7ab7eafea825.json
```

### check directory structure:

```bash
tree mysterious-image
```

Source:
* https://github.com/saschagrunert/demystifying-containers/blob/master/part3-container-images/post.md
* httpd://docker.com
