# Exercise 57: Service mesh - Create recommendation application

>***Description:***\
>:memo: We will learn how to use service mesh using istio and create recommendation application.
---


## Preparation

### Create project for test application:

```bash
oc new-project test-istio
```

### Install Service mesh

Install following operatiors
* OpenShift Elasticsearch
* Red Hat OpenShift distributed tracing platform
* Kiali Operator (provided by Red Hat)
* Red Hat OpenShift Service Mesh

Deploying the control plane from the web console:
* Create project istio-system
```bash
oc new-project istio-system
```
* Go to Installed Operators
* If necessary, select istio-system from the Project menu
* Click the Red Hat OpenShift Service Mesh Operator. Under Provided APIs, the Operator provides links to create two resource types:
  * ServiceMeshControlPlane
  * ServiceMeshMemberRoll (set your test project `test-istio`)


### Add hosts to hosts file

```bash
sudo echo "192.168.130.11 kiali-istio-system.apps-crc.testing jaeger-istio-system.apps-crc.testing istio-ingressgateway-istio-system.apps-crc.testing grafana-istio-system.apps-crc.testing" >> /etc/hosts
```

## Application

There are three different and super simple microservices
in this system and they are chained together in the following sequence:

customer → preference → recommendation

For now, they have a simple exception handling solution
for dealing with a missing dependent service:
it just returns the error message to the end-user.

## Setup

1. Enable service mesh for all application in project
```bash
oc annotate namespaces test-istio   sidecar.istio.io/inject=true
```

3. Your project need to be added in ServiceMeshMemberRoll for Service mesh (you need cluster admin role)
```bash
oc edit ServiceMeshMemberRoll -n istio-system
```

4. Check
```bash
oc describe namespace test-istio
```

## Create application

1. Clone application git repo:
```bash
git clone https://github.com/redhat-scholars/istio-tutorial.git  ~/istio-tutorial
cd ~/istio-tutorial
```

2. Create applications:
```bash
oc apply -f customer/kubernetes/Deployment.yml
```
```bash
oc create -f customer/kubernetes/Service.yml
```
```bash
oc create -f customer/kubernetes/Gateway.yml
```
```bash
oc apply -f preference/kubernetes/Deployment.yml
```
```bash
oc create -f preference/kubernetes/Service.yml
```
```bash
oc apply -f recommendation/kubernetes/Deployment.yml
```
```bash
oc create -f recommendation/kubernetes/Service.yml
```

3. Add second version for recommendation app:
```bash
oc apply -f recommendation/kubernetes/Deployment-v2.yml
```

4. In the terminal window check service:
```bash
export GATEWAY_URL=$(oc get route istio-ingressgateway -n istio-system -o=jsonpath="{.spec.host}")
```
```bash
curl $GATEWAY_URL/customer
```

```bash
while true; do curl $GATEWAY_URL/customer; sleep 0.3; done
```

5. Check kiali dashboard

```bash
firefox https://kiali-istio-system.apps-crc.testing
```

6. Check grafana
```bash
firefox https://grafana-istio-system.apps-crc.testing/dashboards
```


[go to home](../../../README.md)

[go to next](../exercise58/README.md)

---
source: https://redhat-scholars.github.io/istio-tutorial/
