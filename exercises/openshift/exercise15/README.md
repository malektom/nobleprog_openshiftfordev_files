# Exercise 15: Create application based on code git repository 

>***Description:***\
>:memo: We will learn how to build application image based on code repository using s2i.
---

1. Create application image (using `new-build`) using following parameters:
* name: `cats`
* label: `name=cats`
* code repo: `https://github.com/devops-with-openshift/cotd.git`
* environment variable: `SELECTOR=cats`

<details>
  <summary>show me how to do it</summary>

1. Create application 
```bash 
oc new-build \
   --name='cats' \
   -l name='cats'\
   https://github.com/devops-with-openshift/cotd.git \
   -e SELECTOR=cats
```
</details>

2. Check logs from build 
```bash
oc logs -f bc/cats
```

or start build with higher log level and follow option: 
```bash 
oc start-build --build-loglevel=5 cats --follow
```

3. Check objects in project 
```bash 
oc get all
```

4. Clean (delete all object related to application using label)

<details>
  <summary>show me how to do it</summary>

```bash 
oc delete all -l name=cats
```
</details>


[go to home](../../../README.md)

[go to next](../exercise16/README.md)
