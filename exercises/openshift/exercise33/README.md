# Exercise 33: Recreate strategy

>***Description:***\
>:memo: We will learn how deploy application using recreate strategy in OCP. \
> We will also learn how to set up lifecycle hooks on mid hook example:

```yaml
strategy:
  type: Recreate
  recreateParams:
    pre: {}
    mid: {}
    post: {}
```
---


1. Change the directory
```bash
cd ~/workshop/exercises/openshift/exercise33
```

2. Create the first application
```bash
oc apply -f app-v1/.
```

3. Check all
```bash
oc get all -l app=my-app
```

4. Watch pods
```bash
watch -n 1 -d "oc get pod"
```
or
```bash
oc get pod  --watch-only
```

5. In the second terminal check an application

```bash
HOST=$(oc get route/my-app -o jsonpath='{.status.ingress[0].host}')
while true; \
 do \
   sleep 0.4;\
   curl  http://$HOST/; \
done
```

5. In the third terminal create the second application
```bash
oc apply -f app-v2/.
```

6. Clean (delete all object related to application using label or files)

<details>
  <summary>show me how to do it</summary>

```bash
oc delete all -l app=my-app
```
</details>

[go to home](../../../README.md)

[go to next](../exercise34/README.md)
