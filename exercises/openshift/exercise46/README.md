# Exercise 46: Pod with shared storage between containers

>***Description:***\
>:memo: We will learn how to set up multi-container pod with shared data volume.
---


![MultiContainerPods](images/MultiContainerPods.png)

1. Change the directory
```bash
cd ~/workshop/exercises/openshift/exercise46/
```

2. Create new project `test-multicontainer`.
<details>
  <summary>show me how to do it</summary>

```bash
oc new-project test-multicontainer
```
</details>

2. Create an application based on file `multi-container.yaml`
<details>
  <summary>show me how to do it</summary>

```bash
oc apply -f multi-container.yaml
```
</details>

3. Create a service for deployment `mc1` and port 8080

<details>
  <summary>show me how to do it</summary>

```bash
oc expose deployment mc1 --port=8080
```
</details>

4. Create route for service mc1 with hostname `mc1-$(oc project -q).crc-okd.mnieto.pl`
<details>
  <summary>show me how to do it</summary>

```bash
oc expose service mc1 --hostname=mc1-$(oc project -q).crc-okd.mnieto.pl
```
</details>

5. Check service
```bash
while true; do sleep 0.1;\
 curl $(oc get route/mc1 -o jsonpath='{.status.ingress[0].host}');\
done
```

5. Check logs for container `1st` and `2nd` inside pod.
<details>
  <summary>show me how to do it</summary>

* 1st container:
```bash
oc logs -f mc1-<tab> 1st
```
or
```bash
oc logs -f deployments/mc1 1st
```
* 2nd container:
```bash
oc logs -f mc1-<tab> 2nd
```
or
```bash
oc logs -f deployments/mc1 2nd
```
</details>

6. Print files from container:
* `/opt/bitnami/apache/htdocs/index.html` for `1st`
* `/html/index.html` for `2nd`

Use `exec` command and flag `-c` with container name you want to use.

<details>
  <summary>show me how to do it</summary>

* `/usr/share/nginx/html/index.html` for `1st`:
```bash
oc exec -it mc1-<tab> -c 1st -- cat /opt/bitnami/apache/htdocs/index.html
```
or
```bash
oc exec deployments/mc1 -c 1st -- cat /opt/bitnami/apache/htdocs/index.html
```

* `/html/index.html` for `2nd`
```bash
oc exec -it mc1-<tab> -c 2nd -- cat /html/index.html
```
or
```bash
oc exec deployments/mc1 -c 2nd -- cat /html/index.html
```
</details>

7. Clean
Delete application based on file `multi-container.yaml`
<details>
  <summary>show me how to do it</summary>

```bash
oc delete -f multi-container.yaml
```
</details>

[go to home](../../../README.md)

[go to next](../exercise47/README.md)
