# Exercise 63: Using the PVC

>***Description:***\
>:memo: We will learn how to create and use PVC
---


1. Change the directory
```bash
cd ~/workshop/exercises/openshift/exercise63/
```

2. Create a PVC using file `pv-clame.yaml`
<details>
  <summary>show me how to do it</summary>

```bash
oc apply -f pv-clame.yaml
```
</details>

3. Check
* PVC
```bash
oc get pvc
```
```bash
oc describe pvc task-pv-claim
```
* PV
---
> **Warning**
>
> You need cluster-admin rights !!
---

* login to kubeadmin
```bash
oc login -u kubeadmin
```
* check all pv in cluster:
```bash
oc get pv
```
* check pv bound with our pod:
```bash
oc describe pv $( oc get pvc task-pv-claim | awk '{print $3}' | grep -v VOLUME)
```

4. Create a Pod with PVC using file `pv-pod.yaml` and check status.
<details>
  <summary>show me how to do it</summary>

```bash
oc apply -f pv-pod.yaml
```
```bash
oc describe pod task-pv-pod
```
</details>

5. Clean
```bash
oc delete -f pv-pod.yaml
```
```bash
oc delete -f pv-clame.yaml
```

[go to home](../../../README.md)

[go to next](../exercise64/README.md)
