# Exercise 34: Blue/Green strategy

>***Description:***\
>:memo: We will learn how to deploy application in blue/green strategy.
---

1. Create `blue` application (using `new-app` command) with following parameters:
* name: `blue`
* code repository: `https://github.com/devops-with-openshift/bluegreen#master`

<details>
  <summary>show me how to do it</summary>

```bash
oc new-app https://github.com/devops-with-openshift/bluegreen#master --name=blue
```
</details>

2. Expose service `blue` with name `bluegreen` and hostname `bluegreen.crc-okd.mnieto.pl`
<details>
  <summary>show me how to do it</summary>

```bash
oc expose service blue --name=bluegreen --hostname=bluegreen.crc-okd.mnieto.pl
```
</details>

3. Check application
```bash
curl http://$(oc get route/bluegreen -o jsonpath='{.status.ingress[0].host}')/
```

4. Check application in web browser:
```bash
firefox  http://$(oc get route/bluegreen -o jsonpath='{.status.ingress[0].host}')/
```

5. Create `green` application (using `new-app` command) with following parameters:
* name: `green`
* code repository: `https://github.com/devops-with-openshift/bluegreen#green`

<details>
  <summary>show me how to do it</summary>

```bash
oc new-app https://github.com/devops-with-openshift/bluegreen#green --name=green
```
</details>

6. Change traffic to `green` application
```bash
oc patch route/bluegreen -p '{"spec":{"to":{"name":"green"}}}'
```
```bash
curl http://$(oc get route/bluegreen -o jsonpath='{.status.ingress[0].host}')/
```


6. Rollback traffic to `blue` application
```bash
oc patch route/bluegreen -p '{"spec":{"to":{"name":"blue"}}}'
```
```bash
curl http://$(oc get route/bluegreen -o jsonpath='{.status.ingress[0].host}')/
```

7. Clean (delete all object related to both application using labels)
<details>
  <summary>show me how to do it</summary>

```bash
oc delete all -l app=blue
```
```bash
oc delete all -l app=green
```
</details>

[go to home](../../../README.md)

[go to next](../exercise35/README.md)
