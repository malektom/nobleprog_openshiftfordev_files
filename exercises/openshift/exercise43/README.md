# Exercise 43: Add pod limits as container environment variable

>***Description:***\
>:memo: We will learn  how to set up resource constraints inside
> pod configuration and how it can be visible inside container.
---

1. Change the directory
```bash
cd ~/workshop/exercises/openshift/exercise43/
```

2. Create new project `limit-example`:

<details>
  <summary>show me how to do it</summary>

```bash
oc new-project limit-example
```
</details>


3. Create a Pod with an environment variable with limits (use file `env_limits.yaml`)

<details>
  <summary>show me how to do it</summary>

```bash
oc create -f env_limits.yaml
```
</details>

4. Checking a container environment variable
```bash
oc exec envlimit -- env | grep MEMORY | sort
```

5. Checking limits direct in cgroup:
```bash
kubectl exec envlimit  -- cat /sys/fs/cgroup/memory/memory.limit_in_bytes
```

6. Clean.

Delete pod envlimit
<details>
  <summary>show me how to do it</summary>

```bash
oc delete -f env_limits.yaml --grace-period=0 --force
```
</details>

[go to home](../../../README.md)

[go to next](../exercise44/README.md)
