# Exercise 3: Create new project

>***Description:***\
>:memo: We will learn how to create new project using Web UI and CLI command. 
---


1. Login to cluster over [web UI](https://console-openshift-console.apps-crc.testing/dashboards)  and then create new project `exercise3-project-webui`
```bash
crc console
```
![Create Project](images/okd_create_project.png)

2. Create new application with name `php-sample` and hostname `php.crc-okd.mnieto.pl` using "Developer Catalog" (Add -> Developer Catalog  -> All services -> Builder Images -> php`).

![Create application](images/okd_new_project.png)
![New application](images/okd_new_app.png)

3. Check:

- Build
- Pods
- Services

4. Check all objects in your new project from CLI:
```bash
oc project exercise3-project-webui
oc get all
```
or
```bash
oc get all -n exercise3-project-webui
```


4. Create new project using only `oc` command with:
* name: `exercise3-project-cli`
* description: `This is project for Exercise 3`
* display-name: `Exercise 3 project`


<details>
  <summary>show me how to do it</summary>

```bash
oc new-project exercise3-project-cli \
  --display-name="Exercise 3 project" \
  --description="This is project for Exercise 3"
```
</details>


5. Print all your projects:

* using `get` command:
<details>
  <summary>show me how to do it</summary>

```bash
oc get projects
```
</details>

* using `projects` command:
<details>
  <summary>show me how to do it</summary>

```bash
oc projects
```
</details>


[go to home](../../../README.md)

[go to next](../exercise4/README.md)
