# Exercise 44: Test default limits in project


>***Description:***\
>:memo: We will learn  how to set up default resource constraints inside
> namespace and how it will be used in new created pod.
---

---
>**Warning**
>
>You need cluster-admin rights !!

---


1. Create new project `limit-example` in case you already don't have it:

<details>
  <summary>show me how to do it</summary>

```bash
oc new-project limit-example
```
</details>

2. Change the directory
```bash
cd ~/workshop/exercises/openshift/exercise44/
```

3. Add limits (file `limits.yaml`)
<details>
  <summary>show me how to do it</summary>

```bash
oc apply -f limits.yaml
```
</details>


4. Check limits limits (object `limits`)

<details>
  <summary>show me how to do it</summary>

```bash
oc describe limits
```
</details>

5. Create POD from file `busybox.yaml`

<details>
  <summary>show me how to do it</summary>

```bash
oc apply -f busybox.yaml
```
</details>


6. Check Pod resource constraints (use `describe` command)

<details>
  <summary>show me how to do it</summary>

```bash
oc describe pod busybox
```
</details>

7. Clean project:

```bash
oc delete project limit-example
```

[go to home](../../../README.md)

[go to next](../exercise45/README.md)
