# Exercise 36: Update and rollback

>***Description:***\
>:memo: We will learn how to rollback changes to the previous deployment version.
---

1. Create Cat of the Day application using following parameters:
* name: `cotd`
* label: `app=cotd`
* builder image: `php:7.3`
* code repository: `https://github.com/devops-with-openshift/cotd.git`
* environment variable: `SELECTOR=cats`
* option: `--as-deployment-config=true`

<details>
  <summary>show me how to do it</summary>

```bash
oc new-app \
  --name='cotd' \
  -l app=cotd \
  php:7.3~https://github.com/devops-with-openshift/cotd.git \
  -e SELECTOR=cats \
  --as-deployment-config=true
```
</details>

2. Expose application `cotd` with the same name, label `app=cotd` and hostname `cotd.crc-okd.mnieto.pl`

<details>
  <summary>show me how to do it</summary>

```bash
oc expose service cotd --name=cotd -l app='cotd' --hostname=cotd.crc-okd.mnieto.pl
```
</details>

3. Check all objects for application cotd
```bash
oc get all -l app=cotd
```

4. Check Deployment and application
```bash
oc describe dc cotd
```
```bash
firefox  http://$(oc get route/cotd -o jsonpath='{.status.ingress[0].host}')
```

5. Change env variable (use `set` command) `SELECTOR` to value `cities`, which will trigger deployment

<details>
  <summary>show me how to do it</summary>

```bash
oc set env dc cotd SELECTOR=cities
```
</details>

6. Check application:
```bash
firefox  http://$(oc get route/cotd -o jsonpath='{.status.ingress[0].host}')
```


7. Check rollout history
```bash
oc rollout history dc cotd
```
```bash
oc rollout history dc cotd --revision=1
```
```bash
oc rollout history dc cotd --revision=2
```

8. Rollback to previous version
```bash
oc rollback cotd --to-version=1
```

9. Check Deployment
```bash
oc describe dc cotd
```
```bash
oc set triggers dc cotd
```

10. Enable triggers:

> Warning: the following images triggers were disabled: cotd:latest
You can re-enable them with:
```bash
oc set triggers dc cotd --auto
```


11. Change it again:
```bash
oc set env dc cotd SELECTOR=cities
```

12. Check rollout history:
```bash
oc rollout history dc cotd
```

13. Rollout changes to previous version again:
```bash
oc rollout undo dc cotd --to-revision=1
```

14. Check Deployment
```bash
oc describe dc cotd
```
```bash
oc set triggers dc cotd
```

15. Clean (delete all object related to application using label)

<details>
  <summary>show me how to do it</summary>

```bash
oc delete all -l app=cotd
```
</details>

[go to home](../../../README.md)

[go to next](../exercise37/README.md)
