# Exercise 12:  Get information about horizontalpodautoscalers

>***Description:***\
>:memo: We will learn about k8s api resources and version
> and we will check what versions we can use for Horizontal Pod Autoscaler
---

## Display  all resources and api versions
1. Display all api resources
```bash
oc api-resources
```
* Display all api resources isolated in namespaces:
```bash
oc api-resources --namespaced=true
```

* Display all global api resources
```bash
oc api-resources --namespaced=false
```

2. Display all api versions
```
oc api-versions
```

## Compare changes between version on horizontalpodautoscaler (hpa) resource
1. Display hpa resource

<details>
  <summary>show me how to do it</summary>

```bash
oc explain horizontalpodautoscalers
```
</details>

2. Display options for spec for default version

<details>
  <summary>show me how to do it</summary>

```bash
oc explain hpa.spec
```
</details>

3. Display options for spec for v1 version (use option `--api-version`)

<details>
  <summary>show me how to do it</summary>

```bash
oc explain hpa.spec --api-version=autoscaling/v1
```
</details>

4. Display options for spec for version v2 (use option `--api-version`)

<details>
  <summary>show me how to do it</summary>

```bash
oc explain hpa.spec --api-version=autoscaling/v2
```
</details>

4. Which api version needs to be used to set hpa over memory metrics usage ?


[go to home](../../../README.md)

[go to next](../exercise13/README.md)
