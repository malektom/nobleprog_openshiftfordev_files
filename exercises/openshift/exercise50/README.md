# Exercise 50: Create headless service for nginx application

>***Description:***\
>:memo: We will learn how to create ClusterIP headless service for deployment
---

1. Change the directory
```bash
cd ~/workshop/exercises/openshift/exercise50/
```

2. Create a headless service from file `svc-headless-my-application.yaml`
<details>
  <summary>show me how to do it</summary>

```bash
oc apply -f svc-headless-my-app.yaml
```
</details>

3. Create a Pod with dns tools with:
* name: `dnsutils`
* image: `registry.gitlab.com/greenitnet/dnsutils:latest`
* command `sleep 600`

Use command `run`.

<details>
  <summary>show me how to do it</summary>

```bash
oc run dnsutils \
  --image=registry.gitlab.com/greenitnet/dnsutils:latest \
  -- sleep 600
```
</details>

4. Check return dns values for services my-app and my-app-headless
```bash
oc exec dnsutils -- dig my-app-headless A +search +short
```
```bash
oc exec dnsutils -- dig my-app A +search +short
```

5. Scale `my-app` deployment to 2 replicas:
<details>
  <summary>show me how to do it</summary>

```
oc scale deployment my-app --replicas=2
```
</details>

6. Check number of pods:
```bash
oc get pods
```

7. Check return dns values for services my-app and my-app-headless
```bash
oc exec dnsutils -- dig my-app-headless A +search +short
```
```bash
oc exec dnsutils -- dig my-app A +search +short
```

[go to home](../../../README.md)

[go to next](../exercise51/README.md)
