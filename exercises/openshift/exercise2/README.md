# Exercise 2: Check your cluster 


>***Description:***\
>:memo: We will learn basic options for openshift CLI command 
---

1. Check oc help 
```bash 
oc --help
```

2. Check status 
```bash 
oc status --help 
```

3. Check difference between command showing projects.
```bash 
oc projects
```
vs
```bash
oc get projects 
```

4. Check help for new project
```bash 
oc new-project --help
```

5. Check how to switch projects. 
```bash
oc project
```

6. Check user information 
```bash 
oc whoami --help
```
* print console address 
<details>
  <summary>show me how to do it</summary>

```bash
oc whoami --show-console
```
</details> 

* print API address
<details>
  <summary>show me how to do it</summary>

```bash
oc whoami --show-server
```
</details> 

7. Check help for create new application 
```bash 
oc new-app --help
```

8. Check help for new build 
```bash 
oc new-build --help
```


[go to home](../../../README.md)

[go to next](../exercise3/README.md)
