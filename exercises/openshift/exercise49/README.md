# Exercise 49: Create ClusterIP service for nginx application

>***Description:***\
>:memo: We will learn how to create ClusterIP service for deployment
---

1. Create new project `test-service`
<details>
  <summary>show me how to do it</summary>

```bash
oc new-project test-service
```
</details>

2. Change the directory
```bash
cd ~/workshop/exercises/openshift/exercise49/
```

3. Create a service from file `svc-my-application.yaml`
<details>
  <summary>show me how to do it</summary>

```bash
oc apply -f svc-my-app.yaml
```
</details>

4. Create an application from file `my-application.yaml`
<details>
  <summary>show me how to do it</summary>

```bash
oc apply -f my-app.yaml
```
</details>


5. Start a load-generator
```bash
oc run -i --tty load-generator \
  --rm \
  --image=registry.gitlab.com/greenitnet/images/busybox:latest \
  --restart=Never \
  -- /bin/sh -c "while sleep 0.2;\
   do wget -q -O /dev/null http://my-app;\
   done"
```

6. In the second terminal watch logs
```bash
oc logs -f my-app-<tab>
```
or watch logs from all pods:
```bash
stern my-app
```

7. Stop `load-generator` application by pressing `CTRL-D` in first terminal

[go to home](../../../README.md)

[go to next](../exercise50/README.md)
