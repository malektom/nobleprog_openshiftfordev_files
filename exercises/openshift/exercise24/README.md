# Exercise 24: Docker multi-build in OpenShift

>***Description:***\
>:memo: We will learn how to build image using multi-stage Dockerfile
---

1. Clone git repository
```bash
git clone https://github.com/tszymanski/multi-stage-build.git ~/multi-stage-build
cd ~/multi-stage-build
```

2. Create application
```bash
oc apply -f multistagebuild_go.yaml
```

3. Start build with higher log level and follow option:
```bash
oc start-build mytestbuild-go --build-loglevel=1 --follow
```

4. Download image from OpenShift

  ```bash
  docker login -u $(oc whoami) -p $(oc whoami -t) image-registry.crc-okd.mnieto.pl
  ```
  ```bash
  docker pull image-registry.crc-okd.mnieto.pl/$(oc project -q)/mytestbuildimage-go:latest
  ```

5. Check image

```bash
dive image-registry.crc-okd.mnieto.pl/$(oc project -q)/mytestbuildimage-go:latest
```



6. Create application using new image in OpenShift
```bash
oc new-app mytestbuildimage-go --name=mytestbuild-go -l app=mytestbuild-go
```

7. Expose application
```bash
oc expose svc/mytestbuild-go --hostname mytestbuild-go-test.crc-okd.mnieto.pl
```

8. Check application
```bash
curl http://$(oc get route/mytestbuild-go -o jsonpath='{.status.ingress[0].host}')/
```

9. Clean
* delete all object related to build using label

<details>
  <summary>show me how to do it</summary>

```bash
oc delete all -l app=mytestbuild-go
```
</details>

* delete build using file `multistagebuild_go.yaml`

<details>
  <summary>show me how to do it</summary>

```bash
oc delete -f multistagebuild_go.yaml
```
</details>



[go to home](../../../README.md)

[go to next](../exercise25/README.md)
