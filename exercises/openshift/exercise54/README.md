# Exercise 54: Route with sticky session

>***Description:***\
>:memo: We will learn how to use sticky session in route object
> for our application.
---


1. Check header from my-application
```bash
curl -v http://$(oc get route/my-application -o jsonpath='{.status.ingress[0].host}')
```

2. Settings balance and disable cookie

Available options for balance are:  source, round robin, and leastconn

```bash
oc annotate route my-application haproxy.router.openshift.io/balance=roundrobin
```
```bash
oc annotate route my-application haproxy.router.openshift.io/disable_cookies='true'
```

Checking route configuration:
```bash
oc get routes my-application -o yaml | head -n 10
```

output:
```
apiVersion: route.openshift.io/v1
kind: Route
metadata:
  annotations:
    haproxy.router.openshift.io/balance: roundrobin
    haproxy.router.openshift.io/disable_cookies: "true"
  labels:
    app: go-scratch
  name: my-application
...
```

3. Check headers in response
```bash
curl -v http://$(oc get route/my-application -o jsonpath='{.status.ingress[0].host}')
```

4. Check custom cookie
- enable cookie
```bash
oc annotate route my-application \
  haproxy.router.openshift.io/disable_cookies='false' \
  --overwrite
```
- set custom cookie
```bash
oc annotate route my-application \
  router.openshift.io/cookie_name='nobleprog_workshop'
```

5. Check headers in response
```bash
curl -v http://$(oc get route/my-application -o jsonpath='{.status.ingress[0].host}')
```

more information you can find at [route-specific-annotations](https://docs.openshift.com/dedicated/networking/routes/route-configuration.html#nw-route-specific-annotations_route-configuration)


[go to home](../../../README.md)

[go to next](../exercise55/README.md)
