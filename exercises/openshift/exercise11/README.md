# Exercise 11: Get information about namespace in POD in metadata

>***Description:***\
>:memo: We will learn what is definition of namespace in pod metadata
---

Display namespace definition using explain command:

<details>
  <summary>show me how to do it</summary>

```bash 
oc explain pod.metadata
```
</details>

[go to home](../../../README.md)

[go to next](../exercise12/README.md)
