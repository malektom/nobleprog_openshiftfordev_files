# Exercise 47: Use revers proxy inside a POD

>***Description:***\
>:memo: We will learn how to set up multi-container pod with internal container network communication.
---

![multicontainerwebapp](images/multicontainerwebapp.png)

1. Change the directory
```bash
cd ~/workshop/exercises/openshift/exercise47/
```

2. Create a configmap  based on file `multi-container-web-configmap.yaml` with nginx configuration.

<details>
  <summary>show me how to do it</summary>

```bash
oc apply -f multi-container-web-configmap.yaml
```
</details>

3. Create an application based on file `multi-container-web.yaml`
<details>
  <summary>show me how to do it</summary>

```bash
oc apply -f multi-container-web.yaml
```
</details>

4. Create a service for pod `mc2` and expose it with hostname `mc2-$(oc project -q).crc-okd.mnieto.pl`
<details>
  <summary>show me how to do it</summary>

```bash
oc expose pod mc2 --port=8080
```
```bash
oc expose service mc2  --hostname=mc2-$(oc project -q).crc-okd.mnieto.pl
```
</details>

5. Check the configuration
```bash
oc describe service mc2

```

6. Check the application
```bash
while true; do sleep 0.1;\
 curl $(oc get route/mc2 -o jsonpath='{.status.ingress[0].host}');\
done
```

7. Check application logs for container `nginx` and `webapp` in pod `mc2`
<details>
  <summary>show me how to do it</summary>

```bash
oc logs -f mc2 nginx
```
```bash
oc logs -f mc2 webapp
```
</details>

8. Clean

```bash
oc delete -f multi-container-web.yaml
```
```bash
oc delete -f multi-container-web-configmap.yaml
```
```bash
oc delete svc,route mc2
```

[go to home](../../../README.md)

[go to next](../exercise48/README.md)
