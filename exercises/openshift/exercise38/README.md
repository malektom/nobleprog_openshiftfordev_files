# Exercise 38: Get information about the variable metrics in HorizontalPodAutoscaler

>***Description:***\
>:memo: We will learn what option we need to use to set
> correct configuration file for HorizontalPodAutoscaler
---

Check what options we can use in hpa (using `explain` command).

<details>
  <summary>show me how to do it</summary>

```bash
oc explain hpa.spec
```
</details>


[go to home](../../../README.md)

[go to next](../exercise39/README.md)
