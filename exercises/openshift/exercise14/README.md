# Exercise 14: Create application based on image

>***Description:***\
>:memo: We will learn how to create application using only application image.
---

1. Create application (using `new-app` command) with following parameters:
* name: `php-test-image`
* label: `name='php-test-image'`
* container image: `registry.gitlab.com/greenitnet/php-docker-test:latest`

<details>
  <summary>show me how to do it</summary>

```bash
oc new-app \
  --name='php-test-image' \
  -l name='php-test-image' \
  registry.gitlab.com/greenitnet/php-docker-test:latest
```
</details>

2. Check all objects connected with application (check and use labels).

<details>
  <summary>show me how to do it</summary>

```bash
oc get all --show-labels
```
```bash
oc get all -l name=php-test-image
```
</details>

3. Create route with:
* service `php-test-image`
* port 8080
* hostname: php-test-image.crc-okd.mnieto.pl

<details>
  <summary>show me how to do it</summary>

```bash
oc expose \
  svc/php-test-image \
  --port=8080 \
  --hostname=php-test-image.crc-okd.mnieto.pl
```
</details>


4. Print route: 

```bash
oc get route
```

5. Check application 
```
curl -v http://$(oc get route/php-test-image -o jsonpath='{.status.ingress[0].host}')/randtest.php
```

7. Clean (delete all object related to application using label)

<details>
  <summary>show me how to do it</summary>

```bash 
oc delete all -l name=php-test-image
```
</details>

[go to home](../../../README.md)

[go to next](../exercise15/README.md)
