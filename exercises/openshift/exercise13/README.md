# Exercise 13: Liveness and readiness probe

>***Description:***\
>:memo: We will learn  how to use liveness
> and readiness probe in microservice architecture.
---

## Create application hello world writing to redis:

1. Create Pods using yaml filed placed remote resource: `https://gitlab.com/greenitnet/hello_world_flask/-/raw/master/kuberentes_files/hello_world_flask.yaml`. Use command `oc apply`.

<details>
  <summary>show me how to do it</summary>

```bash
oc apply -f https://gitlab.com/greenitnet/hello_world_flask/-/raw/master/okd_files/hello_world_flask.yaml
```
</details>

2. Display logs for myapp pod

<details>
  <summary>show me how to do it</summary>

```bash
oc logs -f myapp<tab>
```
or
```bash
oc logs -f  deployment/myapp
```
</details>

3. Display events.

<details>
  <summary>show me how to do it</summary>

```bash
oc get events -w
```
</details>

4. In the second terminal print description for pod

<details>
  <summary>show me how to do it</summary>

```bash
oc describe pod myapp<tab>
```
</details>

5. Create Pod with redis from remote resource `https://gitlab.com/greenitnet/hello_world_flask/-/raw/master/kuberentes_files/hello_world_redis.yaml`. Use command: `oc apply`:

<details>
  <summary>show me how to do it</summary>

```bash
oc apply -f https://gitlab.com/greenitnet/hello_world_flask/-/raw/master/okd_files/hello_world_redis.yaml
```
</details>

6. Expose service http with hostname `http-exercise3-project-cli.crc-okd.mnieto.pl` and check application:
```bash
oc expose service http --hostname=http-exercise3-project-cli.crc-okd.mnieto.pl
```

7. Check application
```bash
curl http://$(oc get route/http -o jsonpath='{.status.ingress[0].host}')
```

8. Check status of pod my-app:

<details>
  <summary>show me how to do it</summary>

```bash
oc describe pod myapp-<tab>
```
</details>

9. Clean

Delete all objects connected to `myapp` and `redis`.

<details>
  <summary>show me how to do it</summary>

```bash
oc delete -f https://gitlab.com/greenitnet/hello_world_flask/-/raw/master/okd_files/hello_world_flask.yaml
```
```bash
oc delete -f https://gitlab.com/greenitnet/hello_world_flask/-/raw/master/okd_files/hello_world_redis.yaml
```
</details>

[go to home](../../../README.md)

[go to next](../exercise14/README.md)
