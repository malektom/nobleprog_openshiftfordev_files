# Exercise 16: Create application based on Dockerfile 

>***Description:***\
>:memo: We will learn how to build application image based on Dockerfile using s2i.
---

1. Create application image (using `new-build`) using following parameters:
* name: `php-test`
* label: `name=php-test`
* git repo with Dockerfile: `https://gitlab.com/greenitnet/php-docker-test.git`

<details>
  <summary>show me how to do it</summary>

```bash 
oc new-build \
  --name='php-test' \
  -l name='php-test' \
  https://gitlab.com/greenitnet/php-docker-test.git
```
</details>

2. Check logs 
```bash 
oc logs -f bc/php-test
```

or start new build with higher log level and follow option 
```bash 
oc start-build --build-loglevel 5 php-test --follow
```

3. Check objects in projects
```bash 
oc get all --show-labels
```

4. Clean  (delete all object related to application using label)

<details>
  <summary>show me how to do it</summary>

```bash 
oc delete all -l name=php-test
```
</details>

[go to home](../../../README.md)

[go to next](../exercise17/README.md)
