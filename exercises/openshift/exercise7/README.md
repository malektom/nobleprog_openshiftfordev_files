# Exercise 7: Start application from yaml file (declarative approach)

>***Description:***\
>:memo: We will learn how to create application based on configuration file (declarative approach)
---

1. Change the directory
```bash
cd ~/workshop/exercises/openshift/exercise7/
```
2. Create Pod from file
```bash
oc create -f busybox.yaml
```

3. Check pods 

<details>
  <summary>show me how to do it</summary>

```bash
oc get pods
```
</details>

4. Check pod labels
<details>
  <summary>show me how to do it</summary>

```bash 
oc get pod --show-labels
```
</details>

[go to home](../../../README.md)

[go to next](../exercise8/README.md)
