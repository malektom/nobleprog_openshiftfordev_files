# Exercise 45: Test quota in project

>***Description:***\
>:memo: We will learn  how to set up resource quota constraints inside
> project.
---

---
>**Warning**
>
>You need cluster-admin rights !!

---

1. Create new project `quota-example`:
<details>
  <summary>show me how to do it</summary>

```bash
oc new-project quota-example
````
</details>

2. Change the directory:
```
cd ~/workshop/exercises/openshift/exercise45/
```

3. Create a quota from file `quota.yaml` and check it.
<details>
  <summary>show me how to do it</summary>

```bash
oc apply -f quota.yaml
```
```bash
oc describe quota
```
</details>

4. Create a pod from file `busybox.yaml`:

<details>
  <summary>show me how to do it</summary>

```bash
oc create -f busybox.yaml
```
</details>

5. Create default limits from file `limits.yaml`.

<details>
  <summary>show me how to do it</summary>

```bash
oc apply -f limits.yaml
```
```bash
oc describe limits
```
</details>

6. Create one more time pod busybox from file `busybox.yaml`.
<details>
  <summary>show me how to do it</summary>

```bash
oc create -f busybox.yaml
```
</details>

7. Check a quota (object `quota`)
<details>
  <summary>show me how to do it</summary>

```bash
oc describe quota
```
</details>

8. Create a deployment with application nginx with 5 replicas
* In the first terminal print events stream:
```bash
oc get events --watch
```

* In the second terminal create application:
```bash
oc create deployment nginx-test \
  --image=bitnami/nginx:1.21 \
  --replicas=5
```

9. Clean:

* applications:

```bash
oc delete all -l app=nginx-test
```
```bash
oc delete -f busybox.yaml
```

* quota:
```bash
oc delete -f quota.yaml
```

* limits:
```bash
oc delete -f limits.yaml
```


## Quota Scopes

Each quota can have an associated set of scopes. A quota will only measure usage for a resource if it matches the intersection of enumerated scopes.

Adding a scope to a quota restricts the set of resources to which that quota can apply. Specifying a resource outside of the allowed set results in a validation error.

Type of scopes:


|     Scope	 |   Description |
| -------------- | ------------------------------------------------------------------------------ |
| Terminating    | Match pods where spec.activeDeadlineSeconds >= 0.|
| NotTerminating | Match pods where spec.activeDeadlineSeconds is nil. |
| BestEffort     | Match pods that have best effort quality of service for either cpu or memory. See the Quality of Service Classes for more on committing compute resources. |
| NotBestEffort  | Match pods that do not have best effort quality of service for cpu and memory. |



Scope restricts a quota to limiting the following resources:

| Scope          | Resources |
| -------------- | --------- |
| BestEffort     | Pod       |
| Terminating, NotTerminating, and NotBestEffort | pods, memory, requests.memory, limits.memory, cpu, requests.cpu, limits.cpu, ephemeral-storage, requests.ephemeral-storage, limits.ephemeral-storage |


1. Create quota for BestEffort  and NonBestEffort:

* create quota for BestEffort from file `quota_besteffort.yaml`:
```bash
oc create -f quota_besteffort.yaml
```

* create quota for NonBestEffort from file `quota_notbesteffort.yaml`:
```bash
oc create -f quota_notbesteffort.yaml
```

2. print events:
```bash
oc get events --watch
```

3. in the second terminal create application from file `nginx_besteffort.yaml`:

```bash
oc create -f nginx_besteffort.yaml
```

4. Scale deployment to 3 replicas:
```bash
oc scale deployment nginx-besteffort --replicas 3
```

5. Create application from file `nginx_guaranteed.yaml`:
```bash
oc create -f nginx_guaranteed.yaml
```

6. Clean env by deleting project `quota-example`:
<details>
  <summary>show me how to do it</summary>

```bash
oc delete project quota-example
```
</details>

[go to home](../../../README.md)

[go to next](../exercise46/README.md)
