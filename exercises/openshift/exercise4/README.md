# Exercise 4: Start app from command line using CLI (imperative approach)

>***Description:***\
>:memo: We will learn how to create application using command line tool.
---


## Create Pod

* Check options for run command 
```bash
oc run -h
```

* Create Pod using:
* image: registry.gitlab.com/greenitnet/images/busybox:latest
* command:  `sleep` 
* command argument: `3600`
* image-pull-policy: `IfNotPresent`


<details>
  <summary>show me how to do it</summary>

```bash
oc run \
  busybox-cli \
  --image=registry.gitlab.com/greenitnet/images/busybox:latest \
  --image-pull-policy=IfNotPresent \
  sleep 3600
```
</details>

* Check objects in current namespace
```bash
oc get all
```

* Check objects in all namespaces  

<details>
  <summary>show me how to do it</summary>

```bash
oc get all --all-namespaces
```
or
```bash 
oc get all -A
```
</details>


[go to home](../../../README.md)

[go to next](../exercise5/README.md)
