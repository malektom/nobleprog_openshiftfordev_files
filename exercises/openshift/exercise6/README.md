# Exercise 6: Add labels

>***Description:***\
>:memo: We will learn how to display, add, remove labels from object
---


1. Print all pods with information about labels 
```bash
oc get pod --show-labels
```

2. Add label to existing Pod
```bash 
oc label pod busybox-cli app=busybox
```

3. Print all pods with labels again 
```bash 
oc get pod --show-labels
```

4. Delete label from pod
```bash
oc label pod busybox-cli app-
```

[go to home](../../../README.md)

[go to next](../exercise7/README.md)
