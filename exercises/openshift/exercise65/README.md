# Exercise 65: Template

>***Description:***\
>:memo: We will learn how to use template object to automate
> building application stack based on definition templates.\
> We will use go-scratch application from exercise12 and do
> all steps using one action.
---

1. Download template
```bash
mkdir ~/template && cd ~/template
```
```bash
curl \
https://gitlab.com/greenitnet/go-scratch-template/-/raw/master/go-scratch-template.yaml \
 -o go-scratch-template.yaml
```

* Add hostname to route:
```diff
 - apiVersion: v1
   kind: Route
   metadata:
     name: ${NAME}-my-application
   spec:
-    host:
+    host: ${NAME}-my-application.crc-okd.mnieto.pl
     port:
       targetPort: 8080-tcp
     to:
```


2. Add template to your project
```bash
oc apply -f go-scratch-template.yaml
```

3. Check parameters
```bash
oc process --parameters go-scratch
```

4. Create application from web UI  (Add -> Developer Catalog -> All services -> Templates -> Simple Go HTTP Server)
```bash
crc console
```

![Simple Go HTTP server](images/simple_go_http_server.png)


5. Check created objects and new application:
* pods:
```bash
oc get all -l app=my-go-app-from-scrach-my-application
```
* other objects:
```bash
oc get all -l template=go-scratch
```
* Check application:
```bash
curl $(oc get route/my-go-app-from-scrach-my-application -o jsonpath='{.status.ingress[0].host}')
```

6. Delete application:
```bash
oc delete all -l  template=go-scratch
```

[go to home](../../../README.md)

[go to next](../exercise66/README.md)
