# Exercise 51: Service my-service -> www.nobleprog.pl

>***Description:***\
>:memo: We will learn how to create external service
---

1. Change the directory
```bash
cd ~/workshop/exercises/openshift/exercise51/
```

2. Create an ExternalName service from file `service-external-name.yaml`
<details>
  <summary>show me how to do it</summary>

```bash
oc apply -f service-external-name.yaml
```
</details>

3. Print all services:
```bash
oc get svc
```

4. Check return dns values for services `my-service`
```bash
oc exec dnsutils -- dig my-service A +search
```
output
```
my-service.test-network.svc.cluster.local. 30 IN CNAME www.nobleprog.pl
www.nobleprog.pl.	60	IN	A	35.158.142.198
```

5. Connect to ExternalName service:
```bash
oc exec -it dnsutils -- curl -v -H "Host: www.nobleprog.pl"  http://my-service -o /dev/null
```


6. Clean
```bash
oc delete pod dnsutils
```


[go to home](../../../README.md)

[go to next](../exercise52/README.md)
