# Exercise 75: Application logs processing

>***Description:***\
>:memo: We will learn how application logs are process from pod to kibana
---

---
>***Notice***
>
>You need more than one nodes in cluster
>
> login to OCP cluster
>
```bash
oc login -u your_user  https://api.okdnobleprog.gcp.mnieto.pl:6443
```
---

1. Create new project:
```bash
oc new-project $(oc whoami)-logs
```

2.  Create application using `new-app` command with following parameters:
* name: `test-apache-json`
* code repository: `registry.gitlab.com/greenitnet/php-docker-test:latest`
* label: `app=test-apache-json`
* environment variable: `multiplier=100`

<details>
  <summary>show me how to do it</summary>

```bash
oc new-app registry.gitlab.com/greenitnet/php-docker-test:latest \
  -l app=test-apache-json \
  -e multiplier=100 \
  --name=test-apache-json
```
</details>

3. Expose and check application
```bash
oc expose svc test-apache-json --port=8080
```
```bash
host=$(oc get route/test-apache-json -o jsonpath='{.status.ingress[0].host}')
curl http://${host}/randtest.php
```

4. Check logs in terminal
```bash
oc logs --tail 1 test-apache-json-<tab> | jq
```

5. If we have turned on logs on CRC go to kibana and check application logs there
   or visit kibana (in case cluster OCP) https://kibana-openshift-logging.apps.okdnobleprog.gcp.mnieto.pl/

6. Clean
```bash
oc delete all -l app=test-apache-json
```

[go to home](../../../README.md)

[go to next](../exercise76/README.md)
