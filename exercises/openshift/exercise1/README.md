# Exercise 1: Run CodeReadyContainers

>***Description:***\
>:memo: We will learn how to set up CodeReadyContainers
---

1. Download your personal pull-secret.json from https://console.redhat.com and save it to /home/nobleprog/crc/pull-secret.json 


2. Start CodeReadyContainers

* run CodeReadyContainers setup to prepare system requirements
```bash
crc setup
```
* print current CRC configuration
```bash
crc config view
```
* set 6 cpus
```bash
crc config set cpus 6
```
* set 16 GB RAM 
```bash
crc config set memory 20480
```
* set 50 GB disk space
```bash
crc config set disk-size 50
```
* enable monitoring 
```bash 
crc config set enable-cluster-monitoring true
```
* set pull secret 
```bash 
crc config set pull-secret-file /home/nobleprog/crc/pull-secret.json
```
* print CRC configuration 
```bash
crc config view 
```
* if everything is fine start CRC
```bash 
crc start 
```

output: 
```
Started the OpenShift cluster.

The server is accessible via web console at:
  https://console-openshift-console.apps-crc.testing

Log in as administrator:
  Username: kubeadmin
  Password: 6YFGa-uGZj4-KQfuP-LnIZw

Log in as user:
  Username: developer
  Password: developer

Use the 'oc' command line interface:
  $ eval $(crc oc-env)
  $ oc login -u developer https://api.crc.testing:6443
```

3. Checking installation
```bash
oc get nodes
```
or using crc 
```bash
eval $(crc oc-env)
oc get nodes 
```

4. Check CRC server
* Connect to crc server:
```bash 
ssh -i ~/.crc/machines/crc/id_ecdsa core@192.168.130.11
```

* Pring all containers/pods
```bash
sudo runc list
sudo crictl ps
sudo crictl pods
```


[go to home](../../../README.md)

[go to next](../exercise2/README.md)
