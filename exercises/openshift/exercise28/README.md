# Exercise 28: Create DaemonSet

>***Description:***\
>:memo: We will learn what is DaemonSet on example of flunetd
---

1. Create new project
```bash
oc new-project test-deamonset
```

2. Change the directory
```bash
cd ~/workshop/exercises/openshift/exercise28/
```

3. Create a POD
```bash
oc apply -f deamonset-nginx.yaml
```

4. Check a POD 
```bash
oc get pods -o wide 
```

print describe: 
```
oc describe pod nginxds-<tab>
```

5. Clean and remove project. 
```bash
oc delete -f deamonset-nginx.yaml
```
```bash
oc delete project test-deamonset
```

[go to home](../../../README.md)

[go to next](../exercise29/README.md)
