# Exercise 30: Cronjob with cowsay application

>***Description:***\
>:memo: We will learn how we can use Cronjob using our cowsay application
---

1. Create project `test-job`

<details>
  <summary>show me how to do it</summary>

```bash
oc new-project test-job
```
</details>

2. Create Cronjob using your cowsay image (use `create` command)

Use following parameters:
* image: your own cowsay image or `registry.gitlab.com/greenitnet/cowsays:latest`
* set schedule to: `"*/1 * * * *"`
* set restart: `OnFailure`
* Set empty command and the end of your command: `-- `

<details>
  <summary>show me how to do it</summary>

```bash
oc create cronjob \
  cowsay --image=registry.gitlab.com/greenitnet/cowsay:latest \
  --schedule='*/1 * * * *' \
  --restart=OnFailure  \
  --
```
</details>

3. Watch pods created by Cronjob
```bash
watch oc get pods
```
or
```bash
oc get pods -w 
```

4. Check jobs 
```bash 
oc get cronjobs
```
```bash
oc get jobs
```

5. Get logs from pods
```bash 
oc logs <cronjob pod>
```

![cowsay](images/cowsay.png)

5. Clean 
Delete Cronjob: 
```bash 
oc delete cj cowsay
```
Delete project:
```bash 
oc delete project test-job
```

[go to home](../../../README.md)

[go to next](../exercise31/README.md)
