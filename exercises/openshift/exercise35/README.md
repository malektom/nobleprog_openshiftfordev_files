# Exercise 35: Canary Strategy

>***Description:***\
>:memo: We will learn how to deploy application in canary strategy.
---

1. Create Cat of the Day application using following parameters:
* name: `cats`
* label: `app=cats`
* builder image: `php:7.3`
* code repository: `https://github.com/devops-with-openshift/cotd.git`
* environment variable: `SELECTOR=cats`

<details>
  <summary>show me how to do it</summary>

```bash
oc new-app \
 --name='cats' \
 -l app='cats'\
 php:7.3~https://github.com/devops-with-openshift/cotd.git \
 -e SELECTOR=cats
```
</details>

2. Expose service `cats` with name `cats`, label `app=cats` and hostname `cats.crc-okd.mnieto.pl`

<details>
  <summary>show me how to do it</summary>

```bash
oc expose service cats \
  --name=cats \
  -l app='cats' \
  --hostname=cats.crc-okd.mnieto.pl
```
</details>

3. Check cats application
```bash
curl http://$(oc get route/cats -o jsonpath='{.status.ingress[0].host}')/
```
```bash
firefox  http://$(oc get route/cats -o jsonpath='{.status.ingress[0].host}')/
```

4. Create City of the Day application using following parameters:
* name: `city`
* label: `app=city`
* builder image: `php:7.3`
* code repository: `https://github.com/devops-with-openshift/cotd.git`
* environment variable: `SELECTOR=cities`

<details>
  <summary>show me how to do it</summary>

```bash
oc new-app \
  --name='city' \
  -l app='city' \
  php:7.3~https://github.com/devops-with-openshift/cotd.git \
  -e SELECTOR=cities
```
</details>

5. Expose service `city` with the same name, label `app=city` and hostname `city.crc-okd.mnieto.pl`

<details>
  <summary>show me how to do it</summary>

```bash
oc expose service city --name=city -l app='city' --hostname=city.crc-okd.mnieto.pl
```
</details>

6. Check city application:
```bash
curl http://$(oc get route/city -o jsonpath='{.status.ingress[0].host}')/
```
```bash
firefox  http://$(oc get route/city -o jsonpath='{.status.ingress[0].host}')/
```

7. Check route
```bash
oc get routes
```

8. Create new route with name `ab` for service `cats` and hostname `ab.crc-okd.mnieto.pl`


```bash
oc expose service cats --name='ab' --hostname=ab.crc-okd.mnieto.pl
```

Set annotate:
```bash
oc annotate route/ab haproxy.router.openshift.io/balance=roundrobin
oc annotate route/ab haproxy.router.openshift.io/disable_cookies='true'
```

Add new `city` backed to route with weight=0:
```bash
oc set route-backends ab cats=100 city=0
```

7. Check traffic
```bash
SITE=$(oc get route/ab -o jsonpath='{.status.ingress[0].host}')
for i in {1..10}; \
  do curl -s http://${SITE}/item.php | grep "data/images/.*/.*\.jpg" | awk '{print $5}'; \
  done
```

8. In second terminal increase city % in route +10% and check traffic in first terminal
```bash
oc set route-backends ab --adjust city=+10%
```
```bash
oc get route
```

9. In second terminal increase city % in route +40% and check traffic in first terminal
```bash
oc set route-backends ab --adjust city=+40%
```
```bash
oc get route
```


10. Clean (delete all object related to both application using labels)
<details>
  <summary>show me how to do it</summary>

```bash
oc delete all -l app=cats
```
```bash
oc delete all -l app=city
```
</details>

[go to home](../../../README.md)

[go to next](../exercise36/README.md)
