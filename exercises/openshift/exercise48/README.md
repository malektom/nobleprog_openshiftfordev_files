# Exercise 48: Application with init container

>***Description:***\
>:memo: We will learn how to set up init-container inside pod
> to prepare data for main application.
---

1. Change the directory
```bash
cd ~/workshop/exercises/openshift/exercise48/
```

2. Create an application based on file `init-containers.yaml`
<details>
  <summary>show me how to do it</summary>

```bash
oc apply -f init-containers.yaml
```
</details>


3. Print all objects
<details>
  <summary>show me how to do it</summary>


```bash
oc get all
```
</details>


4. Create service pod `init-demo` with port 8080  and expose it with hostname `init-$(oc project -q).crc-okd.mnieto.pl`
<details>
  <summary>show me how to do it</summary>

```bash
oc expose pod init-demo --port=8080
```
```bash
oc expose svc init-demo  --hostname=init-$(oc project -q).crc-okd.mnieto.pl
```
</details>

5. Check application
```bash
curl $(oc get route/init-demo -o jsonpath='{.status.ingress[0].host}')
```
```bash
firefox $(oc get route/init-demo -o jsonpath='{.status.ingress[0].host}')
```

6. Check logs from `init-demo` pod, main container `nginx` and for init container `install`.
<details>
  <summary>show me how to do it</summary>

```bash
oc logs init-demo
```
```bash
oc logs init-demo install
```
```bash
oc logs init-demo apache
```
</details>

7. Clean

```bash
oc delete -f init-containers.yaml
```
```bash
oc delete svc,route init-demo
```


[go to home](../../../README.md)

[go to next](../exercise49/README.md)
