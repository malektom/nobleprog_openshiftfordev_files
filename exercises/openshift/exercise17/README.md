# Exercise 17: Create application using binary mode 

>***Description:***\
>:memo: We will learn how to build application image
> streaming application code direct to OCP (binary mode).
---

1. Create application using code git repo using following parameters:
* name: `php-test`
* label: `name=php-test`
* builder image: `php`
* code repository: `https://gitlab.com/greenitnet/php-docker-test.git#s2i`

>You can connect builder image with code repository using `~` like:\
> `build_image`~`code_repo`

<details>
  <summary>show me how to do it</summary>

```bash 
oc new-app \
  --name='php-test' \
  -l name='php-test' \
  php~https://gitlab.com/greenitnet/php-docker-test.git#s2i
```
</details>

2. Expose application service (`php-test`) with:
* path: `/www` 
* hostname: `php-test.crc-okd.mnieto.pl`

<details>
  <summary>show me how to do it</summary>

```bash 
oc expose svc/php-test --path='/www' --hostname=php-test.crc-okd.mnieto.pl
```
</details>

3. Check application
```bash
curl http://$(oc get route/php-test -o jsonpath='{.status.ingress[0].host}')/www/
```


4. Clone code repository to your desktop and change branch 
```bash 
git clone https://gitlab.com/greenitnet/php-docker-test.git ~/php-docker-test  && cd ~/php-docker-test
git checkout s2i
```

5. Change code application e.g. change background image in www/index.html 
- change body to color blue: 
```diff
  }
  </style>
  </head>
- <body>
+ <body style="background-color:blue">

  <h2>List of available tests</h2>
```


6. Start new build from code in your desktop 
```bash 
oc start-build php-test --from-dir="." --follow
```

7. Check application 
```bash 
curl http://$(oc get route/php-test -o jsonpath='{.status.ingress[0].host}')/www/
```

8. Start new build using default configuration 
```bash 
oc start-build php-test
```

9. Wait for new application pod and check application 
```bash 
curl http://$(oc get route/php-test -o jsonpath='{.status.ingress[0].host}')/www/
```

10. Clean (delete all object related to application using label)

<details>
  <summary>show me how to do it</summary>

```bash 
oc delete all -l name='php-test'
``` 
</details>

[go to home](../../../README.md)

[go to next](../exercise18/README.md)
