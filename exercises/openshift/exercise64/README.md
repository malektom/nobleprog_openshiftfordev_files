# Exercise 64: LAMP application

>***Description:***\
>:memo: We will learn how to subpath option for mount
> on example with pod containing WordPress and database
> in separate containers and using the same volume for data.
---

1. Change the directory
```bash
cd ~/workshop/exercises/openshift/exercise64/
```
2. Create an application LAMP using file `test-lamp-subpath.yaml`
<details>
  <summary>show me how to do it</summary>

```bash
oc apply -f test-lamp-subpath.yaml
```
</details>

3. Create route for service wordpress with hostname `wordpress.crc-okd.mnieto.pl`
<details>
  <summary>show me how to do it</summary>

```bash
oc expose svc wordpress --hostname wordpress.crc-okd.mnieto.pl
```
</details>

4. Check wordpress application
```
firefox wordpress.crc-okd.mnieto.pl
```


5. Login to crc and check volume-subpaths
```bash
ssh -i ~/.crc/machines/crc/id_ecdsa core@192.168.130.11
```
```bash
sudo -i
```
```bash
cd /var/lib/kubelet/pods/$(crictl inspectp $(crictl pods | grep wordpress | awk '{print $1}') | jq '.status.metadata.uid' | tr -d '"')
```

6. Exit from crc

7. Clean
```bash
oc delete -f test-lamp-subpath.yaml
```
```bash
oc delete routes wordpress
```

[go to home](../../../README.md)

[go to next](../exercise65/README.md)
