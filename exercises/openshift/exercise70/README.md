# Exercise 70: Start pod with set UID

>***Description:***\
>:memo: We will learn how to use security context
> to overwrite application UID saved in image.
---

---
> **Warning**
>
> You need cluster-admin rights !!
---

---
> **Warning**
>
> This approach will only work from POD in OCP !
---


1. Change the directory
```bash
cd ~/workshop/exercises/openshift/exercise70/
```

2. Create a pod with security context using file `busybox-uid.yaml`
<details>
  <summary>show me how to do it</summary>

```bash
oc create -f busybox-uid.yaml
```
</details>

3. Check
```bash
oc exec -it busybox-uid -- id
```

4. Create a pod with readonly filesystem using file `busybox-readonly.yaml`
<details>
  <summary>show me how to do it</summary>

```bash
oc create -f busybox-readonly.yaml
```
</details>

5. Check
```bash
oc exec -it busybox-readonly -- touch x
```

6. Clean
```bash
oc delete -f busybox-readonly.yaml --grace-period 0 --force
```
```bash
oc delete -f busybox-uid.yaml  --grace-period 0 --force
```


[go to home](../../../README.md)

[go to next](../exercise71/README.md)
