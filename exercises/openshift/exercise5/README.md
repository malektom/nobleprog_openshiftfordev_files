# Exercise 5: Show information about pod restarts

>***Description:***\
>:memo: We will learn how to use custom-columns format to print information about restarts
---

Check option for get command:
```bash
oc get --help
```

Check pod configuration in json form: 
```
oc get pod _pod_name_ -o json
```


Use custom-columns to print:

| name | containers name | restart count |
| ---- |-----------------| ------------- |

<details>
  <summary>show me how to do it</summary>

```bash 
oc get pods -o custom-columns="name:.metadata.name,\
   container.name:.status.containerStatuses.*.name,\
   restart:.status.containerStatuses.*.restartCount"
```
</details>

[go to home](../../../README.md)

[go to next](../exercise6/README.md)
