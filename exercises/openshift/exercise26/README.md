# Exercise 26: Using labels in deployment

>***Description:***\
>:memo: We will learn what role labels plays in k8s based on deployment object.
---

1. Display labels for all objects

<details>
  <summary>show me how to do it</summary>

```bash
oc get all --show-labels
```
</details>


2. Show a relation between all object connected with `test-app` application:
```bash
oc get all \
  -o jsonpath='{range .items[*]}{@.kind}{" -> "}{@.metadata.name}{" -> "}{@.metadata.ownerReferences[0].kind}{" "}{@.metadata.ownerReferences[0].name}{"\n"}{end}' \
  -l app=go-scratch
```

3. Watch pods
```bash
watch -n 1 -d "oc get pod --show-labels"
```

4. In the second terminal delete label
```bash
oc label pod my-application<tab>  app-
```

5. Show all pods objects with labels

```bash 
oc get pods --selector app
```

6. Show again relation between them
```bash
oc get all \
  -o jsonpath='{range .items[*]}{@.kind}{" -> "}{@.metadata.name}{" -> "}{@.metadata.ownerReferences[0].kind}{" "}{@.metadata.ownerReferences[0].name}{"\n"}{end}' \
  -l app=go-scratch
```

and only for deploymentconfig for my-application:
```bash
oc get all \
  -o jsonpath='{range .items[*]}{@.kind}{" -> "}{@.metadata.name}{" -> "}{@.metadata.ownerReferences[0].kind}{" "}{@.metadata.ownerReferences[0].name}{"\n"}{end}' \
  -l deploymentconfig=my-application
```

and for all objects:
```bash
oc get all \
  -o jsonpath='{range .items[*]}{@.kind}{" -> "}{@.metadata.name}{" -> "}{@.metadata.ownerReferences[0].kind}{" "}{@.metadata.ownerReferences[0].name}{"\n"}{end}'
```

7. Add label back

```bash
oc label pod  my-application<tab> app=go-scratch
```

8. What happened with the new POD for test-app ?


[go to home](../../../README.md)

[go to next](../exercise27/README.md)
