# Exercise 8: Delete resources

>***Description:***\
>:memo: We will learn how to delete kubernetes / openshift  objects
---


## Overview:

Resources can be deleted by file names, stdin, resources and names, or by resources and label selector. JSON and YAML formats are accepted.

### Examples:
  * Delete a pod using the type and name specified in pod.json.
  oc delete -f ./pod.json

  * Delete resources from a directory containing kustomization.yaml - e.g. dir/kustomization.yaml.
  oc delete -k dir

  * Delete a pod based on the type and name in the JSON passed into stdin.
  oc pod.json | kubectl delete -f -

  * Delete pods and services with same names "baz" and "foo"
  oc delete pod,service baz foo

  * Delete pods and services with label name=myLabel.
  oc delete pods,services -l name=myLabel

  * Delete a pod with minimal delay
  oc delete pod foo --now

  * Force delete a pod on a dead node
  oc delete pod foo --force

  * Delete all pods
  oc delete pods --all

1.  Delete busy Pod created from file `busybox.yaml`

<details>
  <summary>show me how to do it</summary>

```bash
oc delete -f busybox.yaml
```
or without waiting for graceful shutdown:
```bash
oc delete -f busybox.yaml (--grace-period=0 --force)
```
</details> 

2.  Delete busybox Pod `busybox-cli` created from command line

<details>
  <summary>show me how to do it</summary>

```bash
oc delete pod busybox-cli --grace-period=0 --force
```
</details>

[go to home](../../../README.md)

[go to next](../exercise9/README.md)
