# Exercise 37: Application scale

>***Description:***\
>:memo: We will learn how to scale deployment application
> by changing numbers of pods.
---


## Change your project back to `project-s2i-go-manual`  with my-application (application in go)
<details>
  <summary>show me how to do it</summary>

```bash
oc project project-s2i-go-manual
```
</details>


The are two options to scale the deployment

*  Scale by edit or patch deployment and increase replicas number:
```bash
oc edit deploymentconfigs my-application
```

* Scale by command line
```bash
oc scale dc my-application --replicas=3
```

[go to home](../../../README.md)

[go to next](../exercise38/README.md)
