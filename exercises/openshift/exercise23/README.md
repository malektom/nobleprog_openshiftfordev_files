# Exercise 23: Build go application using s2i

>***Description:***\
>:memo: We will learn how to build application using chain build
> on top of s2i tool. The final image will contain only
> our application without additional compilation tools.
---


1. Create new project `project-s2i-go-manual`
<details>
  <summary>show me how to do it</summary>

```bash
oc new-project project-s2i-go-manual
```
</details>

2. Import s2i builder image
```bash
oc import-image jorgemoralespou/s2i-go --confirm
```

alternative: 
```bash 
oc tag registry.gitlab.com/greenitnet/images/s2i-go:latest s2i-go:latest
```


3. Build new image (use `new-build` command) using new builder image (pulled above)
with following parameters:
* name: `builder`
* builder image: `s2i-go`
* code repository: `https://github.com/jorgemoralespou/ose-chained-builds`
* context dir: `/go-scratch/hello_world`
* label: `app=go-scratch`

<details>
  <summary>show me how to do it</summary>

```bash
oc new-build s2i-go~https://github.com/jorgemoralespou/ose-chained-builds \
 --context-dir=/go-scratch/hello_world \
 --name=builder \
 --labels=app=go-scratch
```
</details>

4. Check logs
```bash
oc logs -f bc/builder
```

5. Build application (use `new-build` command) by copy files from previous image,
using following parameters:
* name: `runtime`
* label: `app=go-scratch`
* source image: `builder`
* source image path: `/opt/app-root/src/go/src/main/main:.`
* dockerfile: `$'FROM scratch\nCOPY main /main\nEXPOSE 8080\nENTRYPOINT ["/main"]'`

<details>
  <summary>show me how to do it</summary>

```bash
oc new-build --name=runtime \
 --labels=app=go-scratch \
 --source-image=builder \
 --source-image-path=/opt/app-root/src/go/src/main/main:. \
 --dockerfile=$'FROM scratch\nCOPY main /main\nEXPOSE 8080\nENTRYPOINT ["/main"]'
```
</details>

6. Check logs
```bash
oc logs -f bc/runtime
```

7. Checking what we have in new image

- Download image from OpenShift
  - version for crc:
    - Create new route for image registry in project openshift-image-registry as kubeadmin:
    ```bash
    oc login -u kubeadmin
    ```
    
     Extract file with ssl cert: 
     ```bash
     cd /home/nobleprog/workshop/exercises/openshift/exercise23/
     ```
     extract zip archive using password ```the name of training company```
     ```bash 
     unzip ssl.zip
     ```
    
    ```bash
    oc -n openshift-image-registry create route reencrypt image-registry \
    --service=image-registry \
    --port='' \
    --key='/home/nobleprog/workshop/exercises/openshift/exercise23/ssl/key.pem' \
    --cert='/home/nobleprog/workshop/exercises/openshift/exercise23/ssl/cert.pem' \
    --hostname=image-registry.crc-okd.mnieto.pl
    ```

    login to docker registry: 
    ```bash 
    docker login -u $(oc whoami) -p $(oc whoami -t) image-registry.crc-okd.mnieto.pl
    ```

    pull image: 
    ```bash
    docker pull image-registry.crc-okd.mnieto.pl/$(oc project -q)/runtime:latest
    ```

- Check image
```bash
dive image-registry.crc-okd.mnieto.pl/$(oc project -q)/runtime:latest
```

8. Create application (use `new-app` command) based on new runtime image with following parameters:
* name: `my-application`
* label: `app=go-scratch`
* image: runtime
* option: `--as-deployment-config`

<details>
  <summary>show me how to do it</summary>

```bash
oc new-app runtime --name=my-application -l app=go-scratch --as-deployment-config
```
</details>

9. Expose application using:
* service: `my-application`
* hostname: `my-application-$(oc project -q).crc-okd.mnieto.pl`) 

<details>
  <summary>show me how to do it</summary>

```bash
oc expose svc/my-application --hostname=my-application-$(oc project -q).crc-okd.mnieto.pl
```
</details>

10. Check application
```bash
curl -v "http://$(oc get route/my-application -o jsonpath='{.status.ingress[0].host}')/"
```

11. Check all objects for application
```bash
oc get all -l app=go-scratch
```

output:
```
NAME                         READY     STATUS    RESTARTS   AGE
pod/my-application-1-6g6x6   1/1       Running   0          2m

NAME                                     DESIRED   CURRENT   READY     AGE
replicationcontroller/my-application-1   1         1         1         2m

NAME                     TYPE        CLUSTER-IP    EXTERNAL-IP   PORT(S)    AGE
service/my-application   ClusterIP   172.30.46.2   <none>        8080/TCP   2m

NAME                                                REVISION   DESIRED   CURRENT   TRIGGERED BY
deploymentconfig.apps.openshift.io/my-application   1          1         1         config,image(runtime:latest)

NAME                                     TYPE      FROM         LATEST
buildconfig.build.openshift.io/builder   Source    Git          1
buildconfig.build.openshift.io/runtime   Docker    Dockerfile   1

NAME                                 TYPE      FROM          STATUS     STARTED         DURATION
build.build.openshift.io/builder-1   Source    Git@115d289   Complete   5 minutes ago   1m8s
build.build.openshift.io/runtime-1   Docker    Dockerfile    Complete   3 minutes ago   21s

NAME                                     DOCKER REPO                                    TAGS      UPDATED
imagestream.image.openshift.io/builder   docker-registry.default.svc:5000/s2i/builder   latest    4 minutes ago
imagestream.image.openshift.io/runtime   docker-registry.default.svc:5000/s2i/runtime   latest    3 minutes ago

NAME                                      HOST/PORT                              PATH      SERVICES         PORT       TERMINATION   WILDCARD
route.route.openshift.io/my-application   my-application-s2i.okd.mnieto.pl            my-application   8080-tcp
```

12. Check BuildConfig
- builder
```bash
oc get bc/builder -o yaml
```

output:
```

...

spec:
  failedBuildsHistoryLimit: 5
  nodeSelector: null
  output:
    to:
      kind: ImageStreamTag
      name: builder:latest

 source:
    contextDir: /go-scratch/hello_world
    git:
      uri: https://github.com/jorgemoralespou/ose-chained-builds
    type: Git
  strategy:
    sourceStrategy:
      from:
        kind: ImageStreamTag
        name: s2i-go:latest
        namespace: s2i
    type: Source
```
- runtime
```bash
oc get bc/runtime -o yaml
```
output: 
```

...

spec:
  failedBuildsHistoryLimit: 5
  nodeSelector: null
  output:
    to:
      kind: ImageStreamTag
      name: runtime:latest

  source:
    dockerfile: |-
      FROM scratch
      COPY main /main
      EXPOSE 8080
      ENTRYPOINT ["/main"]
    images:
    - as: null
      from:
        kind: ImageStreamTag
        name: builder:latest
        namespace: s2i
      paths:
      - destinationDir: .
        sourcePath: /opt/app-root/src/go/src/main/main
    type: Dockerfile
```

13. Check deploymentConf:
```bash
oc get dc/my-application -o yaml
```

output
```

...

template:
    metadata:
      annotations:
        openshift.io/generated-by: OpenShiftNewApp
      creationTimestamp: null
      labels:
        app: go-scratch
        deploymentconfig: my-application
    spec:
      containers:
      - image: docker-registry.default.svc:5000/s2i/runtime@sha256:def0293ca2b6e5b0fc27d4403ebe51e33f4b412d776a11b34c3f018a07b603c1
        imagePullPolicy: Always
        name: my-application
        ports:
        - containerPort: 8080
          protocol: TCP
        resources: {}
        terminationMessagePath: /dev/termination-log
        terminationMessagePolicy: File
      dnsPolicy: ClusterFirst
      restartPolicy: Always
      schedulerName: default-scheduler
      securityContext: {}
      terminationGracePeriodSeconds: 30
...

    triggers:
  - type: ConfigChange
  - imageChangeParams:
      automatic: true
      containerNames:
      - my-application
      from:
        kind: ImageStreamTag
        name: runtime:latest
        namespace: s2i
      lastTriggeredImage: docker-registry.default.svc:5000/s2i/runtime@sha256:def0293ca2b6e5b0fc27d4403ebe51e33f4b412d776a11b34c3f018a07b603c1
    type: ImageChange
```


[go to home](../../../README.md)

[go to next](../exercise24/README.md)
