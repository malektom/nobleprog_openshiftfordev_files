# Exercise 56: Route HTTP rate limiting

>***Description:***\
>:memo: We will learn how to set rate limit for connection from cluster outside to application using route object.
---

1. Turn on rate limit and set limit for 10 http connection in 10 s time frame.
```bash
oc annotate route my-application \
  haproxy.router.openshift.io/rate-limit-connections='true'
```
```bash
oc annotate route my-application \
  haproxy.router.openshift.io/rate-limit-connections.rate-http=10
```

2. Check rate limits:
```bash
host=$(oc get route/my-application -o jsonpath='{.status.ingress[0].host}')
for i in {1..12}; do curl -v http://$host -o /dev/null ; done
```

3. Wait around 20s and check again:
```
curl -v http://$host
```

[go to home](../../../README.md)

[go to next](../exercise57/README.md)
