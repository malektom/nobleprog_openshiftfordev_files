# Exercise 39: Use an HPA based on CPU

>***Description:***\
>:memo: We will learn how to set up hpa over cpu usage.
---

1. Create new project `hpa-test`
<details>
  <summary>show me how to do it</summary>

```bash
oc new-project hpa-test
```
</details>

2. Change the directory
```bash
cd ~/workshop/exercises/openshift/exercise39/
```

3. Create application based on file `php-apache.yaml`
<details>
  <summary>show me how to do it</summary>

```bash
oc apply -f php-apache.yaml
```
</details>

4. Set a Pod autoscaler based on CPU
```bash
oc autoscale deployment php-apache --cpu-percent=50 --min=1 --max=10
```
or
```bash
oc apply -f hpa-v2.yaml
```

5. Start a load-generator (benchmark)
```bash
oc run -i --tty load-generator \
  --rm \
  --image=busybox \
  --restart=Never \
  -- /bin/sh -c "while sleep 0.01; do wget -q -O- http://php-apache/randtest.php; done"
```

6. Watch Pod changes
```bash
watch -d -n 1 "oc get hpa"
```

7. Check pod stats:
```bash
oc adm top pods
```

8. How it looks in v2 version configuration format?
```bash
oc get hpa.v2.autoscaling php-apache -o yaml
```
```bash
oc get hpa.v1.autoscaling php-apache -o yaml
```

9. Clean

Delete hpa `php-apache`
<details>
  <summary>show me how to do it</summary>

```bash
oc delete hpa php-apache
```
</details>

[go to home](../../../README.md)

[go to next](../exercise40/README.md)
