# Exercise 29: Test statefulset application

>***Description:***\
>:memo: We will learn about and how to use Statefulset
---

1. Create new project `test-statefulset`

<details>
  <summary>show me how to do it</summary>

```bash 
oc new-project test-statefulset
```
</details>


2. Change the directory
```bash
cd ~/workshop/exercises/openshift/exercise29/
```

3. Create Statefulset
```bash
oc apply -f my-app-statefulset.yaml
```

4. Check a status
```bash
oc get sts
```
```bash
oc get all
```

5. Check PersistentVolumeClaim
```bash
oc get pvc
```

6. Scale number of pods: 
```bash 
oc scale sts my-app --replicas=5
```

7. Check pods and PersistentVolumeClaim

<details>
  <summary>show me how to do it</summary>

* display pods
```bash
oc get pods 
```
* display PersistentVolumeClaim
```bash 
oc get pvc
```
</details>

5. Clean
Delete statefuleset 
```bash
oc delete -f my-app-statefulset.yaml
```

Delete PVC
```bash
oc delete pvc -l app=nginx
```

Delete project 
```bash
oc delete project test-statefulset
```

[go to home](../../../README.md)

[go to next](../exercise30/README.md)
