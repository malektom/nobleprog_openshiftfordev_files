# Exercise 10: Check default values set for busybox POD

>***Description:***\
>:memo: We will learn how to check default values for pod objects
---

1. Change the directory
```bash
cd ~/workshop/exercises/openshift/exercise10/
```

2. Create a Pod based on image busybox image using `busybox.yaml` file.

<details>
  <summary>show me how to do it</summary>

```bash
oc create -f busybox.yaml
```
</details>

3. Display a Pod configuration and compare it with `busybox.yaml` file. What was added by OCP ? 

<details>
  <summary>show me how to do it</summary>

```bash
oc get pods busybox -o yaml
```

and display pod configuration file:
```bash
cat busybox.yaml
```
</details>



[go to home](../../../README.md)

[go to next](../exercise11/README.md)
