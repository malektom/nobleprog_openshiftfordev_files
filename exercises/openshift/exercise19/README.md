# Exercise 19: Build s2i image in OpenShift

>***Description:***\
>:memo: We will learn how to use OpenShift to build builder image for s2i tool
---

## Test application in desktop

1. Clone repository
```bash
git clone https://github.com/openshift-katacoda/simple-http-server.git ~/simple-http-server
cd ~/simple-http-server
```

2. Build s2i image and application
* s2i image: 
```bash
docker build -t simple-http-server .
```

* application image: 
```bash
s2i build https://gitlab.com/greenitnet/php-docker-test.git simple-http-server simple-http --ref s2i --context-dir www
```

3. Run and check application
* run 
```bash
docker run --rm -d --name simple-http  -p 8080:8080 simple-http
```

* Check:
```bash
curl -v 127.0.0.1:8080
```

* Clean
```bash
docker stop simple-http
```

## Build S2i image in Openshift

1. Build s2i image (using `new-build` command) with following parameters:
* name: `simple-http-server`
* strategy: `docker`
* code repository: `https://github.com/openshift-katacoda/simple-http-server`

<details>
  <summary>show me how to do it</summary>

```bash
oc new-build \
   --strategy=docker \
   --name simple-http-server \
   --code https://github.com/openshift-katacoda/simple-http-server
```
</details>

2. Check objects in our projects:
```bash
oc get all
```

3. Create application (use `new-app` command) using new s2i image using following parameters:
* name: `php-test`
* builder image: `simple-http-server`
* code repository: `https://gitlab.com/greenitnet/php-docker-test.git#s2i`
* context dir: `www`

<details>
  <summary>show me how to do it</summary>

```bash
oc new-app \
   -l name=php-test \
   simple-http-server~https://gitlab.com/greenitnet/php-docker-test.git#s2i \
   --context-dir www
```
</details>

4. Check objects:
```bash
oc get all
```

5. Expose route `php-docker-test` with hostname `php-docker-test.crc-okd.mnieto.pl`

<details>
  <summary>show me how to do it</summary>

```bash
oc expose \
  svc/php-docker-test \
  --hostname=php-docker-test.crc-okd.mnieto.pl
```
</details>


6. Check application
```bash
curl http://$(oc get route/php-docker-test -o jsonpath='{.status.ingress[0].host}')
```

8. Clean (delete all object related to application and build using label)

<details>
  <summary>show me how to do it</summary>

```bash
oc delete all -l name=php-test
oc delete all -l build=simple-http-server 
```
</details>

[go to home](../../../README.md)

[go to next](../exercise20/README.md)
