# Exercise 31: Create a POD with a configmap as volume

>***Description:***\
>:memo: We will learn how to use configmap on example with nginx configuration file mounted as volume
---

1. Create namespace test-configmap

<details>
  <summary>show me how to do it</summary>

```bash
oc new-project test-configmap
```
</details>

2. Change the directory
```bash
cd ~/workshop/exercises/openshift/exercise31/
```

3. Create a configmap from file `my-app-nginx-configmap.yaml`

<details>
  <summary>show me how to do it</summary>

```bash
oc apply -f my-app-nginx-configmap.yaml
```
</details>

4. Check
```bash
oc get configmap my-app-nginx-conf -o yaml
```

5. Create a deployment with nginx application using file `my-app-nginx.yaml`

<details>
  <summary>show me how to do it</summary>

```bash
oc apply -f my-app-nginx.yaml
```
</details>

6. Check
```bash
oc describe pod my-application-<tab>
```

7. Create service and route with hostname `my-application-$(oc project -q).crc-okd.mnieto.pl` for deployment `my-application` with port `8080`:

<details>
  <summary>show me how to do it</summary>

```bash
oc expose deployment my-application --port=8080
```
```bash
oc expose svc/my-application --hostname=my-application-$(oc project -q).crc-okd.mnieto.pl
```
</details>

8. In the second window check site
```bash
curl -v "http://$(oc get route/my-application -o jsonpath='{.status.ingress[0].host}')/"
```

9. Clean. Remove project test-configmap

<details>
  <summary>show me how to do it</summary>

```bash
oc delete project test-configmap
```
</details>

[go to home](../../../README.md)

[go to next](../exercise32/README.md)
