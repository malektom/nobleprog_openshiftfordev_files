# Exercise 32: Rolling Strategy


>***Description:***\
>:memo: We will learn how to deploy application using rolling strategy.
---


1. Change the directory
```bash
cd ~/workshop/exercises/openshift/exercise32
```

2. Create namespace test-strategy
```bash
oc new-project test-strategy
```

3. Create the first application
```bash
oc apply -f app-v1/.
```

4. Check objects for this application:
```bash
oc get all -l app=my-app
```

5. Watch pods
```bash
watch -n 1 -d "oc get pod -l app=my-app "
```
or
```bash
oc get pod -l app=my-app --watch-only
```

6. In the second terminal check an application
```bash
while true; do sleep 0.4;\
 curl http://$(oc get route/my-app\
 -o jsonpath='{.status.ingress[0].host}')/;\
done
```

7. In the third terminal create the second application
```bash
oc apply -f app-v2/.
```

8. Clean  (delete all object related to application using label or files)

<details>
  <summary>show me how to do it</summary>

```bash
oc delete all -l app=my-app
```
</details>

[go to home](../../../README.md)

[go to next](../exercise33/README.md)
