# Exercise 9: Get information about all Pods configuration options

>***Description:***\
>:memo: We will learn how to display details information
> about k8s objects configuration using `explain` command
---


1. List the fields for supported resources.

 This command describes the fields associated with each supported API resource.
 Fields are identified via a simple JSONPath identifier: 
 kubectl explain `<type>`.`<fieldName>[.<fieldName>]`
 like: 

```bash
oc explain pod
```

2. Get information about POD specification for `imagePullPolicy`:

<details>
  <summary>show me how to do it</summary>

```bash
kubectl explain pod.spec.containers.imagePullPolicy
```
</details>


[go to home](../../../README.md)

[go to next](../exercise10/README.md)
